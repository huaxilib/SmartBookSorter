# -*-coding:utf-8-*-
import os
import shutil
from common import config
from common import sqlite

__author__ = 'Jason'

def listDir(dir):
    files = []
    fielnum =0
    list = os.listdir(dir)#列出目录下的所有文件和目录
    for line in list:
        filepath = os.path.join(dir,line)
        if os.path.isdir(filepath):#如果filepath是目录，则再列出该目录下的所有文件
            listDir(filepath)
            """for li in os.listdir(filepath):
                files.append(filepath)
                fielnum = fielnum + 1"""
        elif os.path:#如果filepath是文件，直接列出文件名
            files.append(filepath)
            print(filepath)
            fielnum = fielnum + 1

    print('文件数: '+ str(fielnum))
    return  files

def createBooksDir(path):
    cates = sqlite.loadBookCategroy(config.getMainDb())
    for (key,value) in cates.items():
        createBookDir(cates,path,key)
    return

# sort_key 分类号
def createBookDir(cates, path, sort_key):
    returnPath = ""
    tempPath = ""

    levelOne = ""
    key_len = len(sort_key)
    if key_len > 0:
        if not sort_key[0:1] in cates:
            print("错误的分类号")
            return returnPath
        else:
            levelOne = "/" + sort_key[0:1] + cates[sort_key[0:1]]
            print("")
    else:
        print("错误的分类号")
        return returnPath

    path = path + levelOne

    while(key_len >= 0):
        key_len -= 1
        if key_len == -1:
            return  returnPath
        if not sort_key in cates:
            sort_key = sort_key[0:key_len]
        else:
            break

    #todo: 这里要修改
    if len(sort_key) == 1:
        returnPath = path
        createDir(returnPath)
        return returnPath
    elif len(sort_key) == 2:
        returnPath = path + "/" + sort_key + cates[sort_key]
        createDir(returnPath)
        return returnPath
    elif len(sort_key) == 3:
        if sort_key[0:2] in cates:
            returnPath = path + "/" + sort_key[0:2] + cates[sort_key[0:2]] + "/" + sort_key + cates[sort_key]
            createDir(returnPath)
            return returnPath
        else:
            returnPath = (path + "/" + sort_key + cates[sort_key])
            createDir(returnPath)
            return returnPath
    elif len(sort_key) == 4:
        #print(sort_key)
        if sort_key[0:3] in cates:
            if sort_key[0:2] in cates:
                returnPath = (
                    path + "/" + sort_key[0:2] + cates[sort_key[0:2]] + "/" + sort_key[0:3] + cates[sort_key[0:3]] + "/" + sort_key + cates[sort_key])

                createDir(returnPath)
                return returnPath
            else:
                returnPath = (path + "/" + sort_key[0:3] + cates[
                    sort_key[0:3]] + "/" + sort_key + cates[sort_key])
                createDir(returnPath)
                return returnPath
        else:
            if sort_key[0:2] in cates:
                returnPath = (
                    path + "/" + sort_key[0:2] + cates[sort_key[0:2]] + "/" + sort_key + cates[sort_key])
                createDir(returnPath)
                return returnPath
            else:
                returnPath = (
                    path + "/" + sort_key + cates[sort_key])
                createDir(returnPath)
                return returnPath
    else:
        #print(sort_key)
        if sort_key[0:4] in cates:
            if sort_key[0:3] in cates:
                if sort_key[0:2] in cates:
                    returnPath = (
                        path + "/" + sort_key[0:2] + cates[sort_key[0:2]] + "/" + sort_key[0:3] + cates[sort_key[0:3]] + "/" + sort_key[0:4] +
                        cates[sort_key[0:4]] + "/" + sort_key + cates[sort_key])
                    createDir(returnPath)
                    return returnPath
                else:
                    returnPath = (
                        path + "/" + sort_key[0:3] + cates[sort_key[0:3]] + "/" + sort_key[0:4] + cates[
                            sort_key[0:4]] + "/" + sort_key + cates[sort_key])
                    createDir(returnPath)
                    return returnPath
            else:
                if sort_key[0:2] in cates:
                    returnPath = (
                        path + "/" + sort_key[0:2] + cates[sort_key[0:2]] + "/" + sort_key[0:4] +
                        cates[sort_key[0:4]] + "/" + sort_key + cates[sort_key])
                    createDir(returnPath)
                    return returnPath
                else:
                    returnPath = (
                        path + "/" + sort_key[0:4] +
                        cates[sort_key[0:4]] + "/" + sort_key + cates[sort_key])
                    createDir(returnPath)
                    return returnPath
        else:
            if sort_key[0:3] in cates:
                if sort_key[0:2] in cates:
                    returnPath = (
                        path + "/" + sort_key[0:2] + cates[sort_key[0:2]] + "/" + sort_key[0:3] + cates[
                            sort_key[0:3]] + "/" + sort_key + cates[sort_key])
                    createDir(returnPath)
                    return returnPath
                else:
                    returnPath = (
                        path + "/" + sort_key[0:3] + cates[sort_key[0:3]] + "/" + sort_key + cates[sort_key])
                    createDir(returnPath)
                    return returnPath
            else:
                if sort_key[0:2] in cates:
                    returnPath = (
                        path + "/" + sort_key[0:2] + cates[sort_key[0:2]] + "/" + sort_key + cates[sort_key])
                    createDir(returnPath)
                    return returnPath
                else:
                    returnPath = (
                        path + "/" + sort_key + cates[sort_key])
                    createDir(returnPath)
                    return returnPath

        finalPath = returnPath
        createDir(finalPath)
        return finalPath

# 创建多级目录
def createDir(dir):
    if not os.path.exists(dir):
        os.makedirs(dir)
    return

def copyFile(filepath,dirPath):
    if not os.path.exists(dirPath + filepath):
        shutil.copy(filepath, dirPath + "\\" + os.path.basename(filepath))
    return

def moveFile(filepath,dirPath):
    createDir(dirPath)

    if not os.path.exists(dirPath + filepath):
        shutil.move(filepath, dirPath + "\\" + os.path.basename(filepath))
    return

def renameFile(filepath,newName):
    if os.path.exists(filepath):
        dir = os.path.dirname(filepath)
        os.rename(filepath,dir + "/" + newName + os.path.splitext(filepath)[1])
    return