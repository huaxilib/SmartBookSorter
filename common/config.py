# -*-coding:utf-8-*-

# 存放未分类图书的地方
import sys

unsorted_dir="c:\\www\\dir"
# 支持的文件类型
support_file_type = ""
# 存放分类图书的地方
sorted_dir="C:\\xxx\\"
# 存放处理后的图书的地方
processedDir = ""
# 分类查询接口地址（如果调用多个，这里的地址会有多个
wenjin_api1="http://find.nlc.cn/search/doSearch?query={0}&secQuery=&actualQuery={0}&searchType=2&docType={1}&mediaTypes=0,1,2&isGroup=isGroup&targetFieldLog={2}&orderBy=RELATIVE"
wenjin_api2="http://find.nlc.cn/search/showDocDetails?docId={0}&dataSource=ucs01"
# 分类API
cate_api1="http://www.ztflh.com/?c={0}"
# 百度云帐号信息及云的存放目录（默认根目录） [可选]
baiduyun_id=""
baiduyun_pwd=""
baiduyun_root="花溪亚历山大图书馆"
# 存放未分类图书的百度云帐号信息 [可选]
unsorted_baiduyun_id=""
unsorted_baiduyun_pwd=""
unsorted_baiduyun_root="花溪亚历山大图书馆"
# 存放未分类图书的微盘帐号信息 [可选]
unsorted_weipan_id=""
unsorted_weipan_pwd=""
unsorted_weipan_root="花溪亚历山大图书馆"

# 内置主数据库
def getMainDb():
    return sys.path[0] + "/data/main.db"

def getRoot():
    return  sys.path[0]
