# -*-coding:utf-8-*-
__author__ = 'Jason'

import win32com.client

def load(file):
    conn = win32com.client.Dispatch(r'ADODB.Connection')
    DSN = 'PROVIDER=Microsoft.Jet.OLEDB.4.0;DATA SOURCE={0};'.format(file)
    conn.Open(DSN)

    rs = win32com.client.Dispatch(r'ADODB.Recordset')
    rs_name = 'BookClass'  # 表名
    rs.Open('[' + rs_name + ']', conn, 1, 3)

    flds = {}
    result = []
    rs.MoveFirst()
    count = 0
    while not rs.EOF:
        item = [0,0]
        for x in range(rs.Fields.Count):
            flds[x] = rs.Fields.Item(x).Value
            item[x] = flds[x]
        result.append(item)
        print("-------------------------")
        print("|   %s|   %s|" % (flds[0], flds[1]))
        count = count + 1
        rs.MoveNext()

    print("Record Count:%d" % (count))

    conn.Close()
    return result