# -*-coding:utf-8-*-

__author__ = 'Jason'


# 一个简单的类
class BookInfo(object):
    def __init__(self, title="",fl_title="",group_title="",pages=0,version="",pub_point="",keywords="",summary="",catalog="",lang="", sort_no=""):
        # 初始化一个新的BookInfo实例
        # 书名
        self.title = title
        # 外文书名
        self.fl_title = fl_title
        # 丛书名
        self.group_title = group_title
        # 分类号
        self.sort_no = sort_no
        # 页数
        self.pages = pages
        # 发行地
        self.pub_point = pub_point
        # 版本
        self.version = version
        # 语种
        self.lang = lang
        # 关键字
        self.keywords = keywords
        # 目录
        self.catalog = catalog
        # 摘要
        self.summary = summary

