# -*-coding:utf-8-*-
import sys
print('================Python import mode==========================');
print ('The command line arguments are:')
for i in sys.argv:
    print (i)
print ('\n The python path',sys.path)

from sys import argv,path  #  导入特定的成员
print('================python from import===================================')
print('path:',path) # 因为已经导入path成员，所以此处引用时不需要加sys.path

# 如果你要使用所有sys模块使用的名字，你可以这样：

from sys import *
print('path:',path)

# 从以上我们可以简单看出：

############################
#导入modules，import与from...import的不同之处在于，简单说：
# 如果你想在程序中用argv代表sys.argv，
# 则可使用：from sys import argv
# 一般说来，应该避免使用from..import而使用import语句，
# 因为这样可以使你的程序更加易读，也可以避免名称的冲突
###########################